function barChart() {
    //-------------
    //- BAR CHART -
    //-------------
    $.ajax({
        url: url + 'dashboard/getDataChart',
        method: 'post',
        dataType: 'json',
        success(data) {
            console.log(data);
            let kecamatan = [];
            let giziburuk = [];
            let stunting = [];
            for (let i = 0; i < data.length; i++) {
                kecamatan.push(data[i].nama_kecamatan);
                giziburuk.push(parseInt(data[i].giziburuk));
                stunting.push(parseInt(data[i].stunting));
            }

            let charData = {
                labels: kecamatan,
                datasets: [{
                    label: 'Gizi Buruk',
                    backgroundColor: 'rgba(60,141,188,0.9)',
                    borderColor: 'rgba(60,141,188,0.8)',
                    pointRadius: false,
                    pointColor: '#3b8bba',
                    pointStrokeColor: 'rgba(60,141,188,1)',
                    pointHighlightFill: '#fff',
                    pointHighlightStroke: 'rgba(60,141,188,1)',
                    data: giziburuk
                },
                {
                    label: 'Stunting',
                    backgroundColor: 'rgba(210, 214, 222, 1)',
                    borderColor: 'rgba(210, 214, 222, 1)',
                    pointRadius: false,
                    pointColor: 'rgba(210, 214, 222, 1)',
                    pointStrokeColor: '#c1c7d1',
                    pointHighlightFill: '#fff',
                    pointHighlightStroke: 'rgba(220,220,220,1)',
                    data: stunting
                },
                ]
            }

            let barChartCanvas = $('#barChart').get(0).getContext('2d')
            let barChartData = jQuery.extend(true, {}, charData)
            let temp0 = charData.datasets[0]
            let temp1 = charData.datasets[1]
            barChartData.datasets[0] = temp1
            barChartData.datasets[1] = temp0


            let barChartOptions = {
                responsive: true,
                maintainAspectRatio: false,
                datasetFill: false,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }

            let barChart = new Chart(barChartCanvas, {
                type: 'bar',
                data: barChartData,
                options: barChartOptions,
            })

        }
    });
}