// $(document).ready(function () {

$('#form').on('submit', function (e) {
    e.preventDefault();
    Swal.fire({
        title: 'Are you sure?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: $(this).attr('action'),
                method: 'post',
                data: new FormData(this),
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                dataType: 'json',
                cache: false,
                beforeSend: function () {
                    $('#btn-submit').attr('disabled', 'disabled');
                    $('#btn-submit').html('<i class="icon-spinner2 spinner"></i> Sedang di proses');
                },
                error: function (e) {
                    Swal.fire('Error', e, 'error');
                    console.log(e);
                },
                complete: function () {
                    $('#btn-submit').removeAttr('disabled');
                    $('#btn-submit').html('<i class="icon-plus2"></i> Save');
                },
                success: function (data) {
                    if (data.error === true) {
                        if (data.msg.error == undefined && data.msg.error == null) {
                            Swal.fire({
                                title: 'Oops...',
                                text: data.msg,
                                icon: 'error'
                            });

                            let keys = Object.keys(data);
                            let value = Object.values(data);
                            for (let i = 1; i < value.length; i++) {
                                if (value[i] !== '') {
                                    $('#' + keys[i]).html(value[i]);
                                } else {
                                    $('#' + keys[i]).html('');
                                }
                            }
                        } else {
                            Swal.fire({
                                title: 'Oops...',
                                text: data.msg.error,
                                icon: 'error'
                            });
                        }
                    } else {
                        if (data.msg.error) {
                            Swal.fire({
                                title: 'Oops...',
                                text: data.msg.error,
                                icon: 'error'
                            });

                        } else {
                            Swal.fire({
                                title: data.msg,
                                icon: 'success'
                            });

                            setTimeout(function () {
                                // window.location.reload();
                                window.location.href = redirect;
                            }, 1000);
                        }
                    }
                }
            });
        } else {
            Swal.fire({
                title: 'Oops...',
                text: 'Failed',
                icon: 'warning'
            });
        }
    });
});

function btn_delete(...args) {
    const data = {
        'textSwal': "To delete this " + args[2],
        'f': args[0],
        'url': args[1],
        'id_kecamatan': args[3],
        'id_kelurahan': args[4],
        'id_puskesmas': args[5],
    };

    btn_action(data);
}

function btn_action(args) {
    Swal.fire({
        title: 'Are you sure?',
        text: args.textSwal,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: url + args.url,
                data: {
                    data: args
                },
                dataType: 'json',
                method: 'post',
                success(data) {
                    if (data.status) {
                        Swal.fire(
                            '',
                            data.msg,
                            'success'
                        );

                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: data.msg,
                        });
                    }
                }
            });
        }
    });
}
// });