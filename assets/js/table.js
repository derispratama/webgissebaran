$(document).ready(function () {
    fill_table();

    function fill_table(arg = '') {
        $('#table').DataTable({
            responsive: true,
            processing: true,
            severSide: true,
            scrollX: true,
            order: [],
            searching: true,
            fixedColumns: {
                leftColumns: 3
            },
            aLengthMenu: [[5, 10, 15, -1], [5, 10, 15, "All"]],
            iDisplayLength: 5,
            ajax: {
                url: urlTable,
                type: 'POST',
                data: {
                    arg
                }
            },
            columnDefs: [{
                target: [0, 3, 4],
                orderable: false,
            },],
        });
    }


    $(document).on('click', '#btn-filter-penyakit', function (e) {
        e.preventDefault();

        const penyakit = $('#fid_penyakit').val();
        $('#table').DataTable().destroy();
        fill_table(penyakit);
    });
});