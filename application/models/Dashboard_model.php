<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard_model extends CI_Model
{
    public function getDataChart()
    {

        return $this->db->query("SELECT tk.nama_kecamatan,tk.jml_terjangkit,SUM(CASE WHEN tt.id_penyakit = '1' THEN 1 ELSE '0' END) as giziburuk, SUM(CASE WHEN tt.id_penyakit = '2' THEN 1 ELSE '0' END) as stunting FROM tbl_kecamatan tk LEFT JOIN tbl_terjangkit tt ON tt.id_kecamatan = tk.id_kecamatan GROUP BY tk.id_kecamatan")->result_array();
    }
}
