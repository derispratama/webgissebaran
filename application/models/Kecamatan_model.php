<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kecamatan_model extends CI_Model
{
    // datatable --------------------------------------------------------------------------------------
    public function make_query()
    {
        $this->db->select('*');
        $this->db->from('tbl_kecamatan tk');

        $this->db->order_by("tk.id_kecamatan", "DESC");
    }

    public function make_datatable()
    {
        $this->make_query();
        $query = $this->db->get();
        return $query->result();
    }

    public function get_filtered_data()
    {
        $this->make_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function get_all_data()
    {
        $this->db->select("*");
        $this->db->from("tbl_kecamatan");
        return $this->db->count_all_results();
    }
    // akhir datatable --------------------------------------------------------------------------------------

    public function all_data()
    {
        $kecamatandetail = $this->db->query("SELECT tk.*,tw.code_warna,CASE WHEN tk.id_warna = 1 THEN 'Aman' WHEN tk.id_warna > 1 AND tk.id_warna < 7 THEN 'Siaga' ELSE 'Bahaya' END as statuskecamatan,SUM(CASE WHEN tt.id_penyakit = '1' THEN 1 ELSE '0' END) as giziburuk_kecamatan, SUM(CASE WHEN tt.id_penyakit = '2' THEN 1 ELSE '0' END) as stunting_kecamatan FROM tbl_kecamatan tk LEFT JOIN tbl_terjangkit tt ON tt.id_kecamatan = tk.id_kecamatan LEFT JOIN tbl_warna tw ON tw.id_warna = tk.id_warna GROUP BY tk.id_kecamatan");

        // $this->db->select('tk.*,tw.code_warna');
        // $this->db->join('tbl_warna tw', 'tw.id_warna = tk.id_warna');
        // $query = $this->db->get('tbl_kecamatan tk');
        return $kecamatandetail->result();

        // $query = $this->db->get('tbl_kecamatan');
        // return $query->result();
    }

    public function form_action($type)
    {
        $data = [
            'nama_kecamatan' => $this->input->post('nama_kecamatan', true),
            'jml_terjangkit' => 0,
            'geojson' => $_FILES['geojson']['name']
        ];

        if ($type == 'add') {
            $data['id_warna'] = 1;
            $this->db->insert('tbl_kecamatan', $data);
            return $this->db->insert_id();
        } else {
            $this->db->where('id_kecamatan', $this->input->post('id_kecamatan', true));
            $this->db->update('tbl_kecamatan', $data);
        }

        if ($this->db->affected_rows() > 0) {
            return $type == 'add' ? $this->db->insert_id() : true;
        } else {
            return false;
        }

        // $this->db->last_query();
    }
}
