<?php
class Terjangkit_model extends CI_Model
{
    // datatable --------------------------------------------------------------------------------------
    public function make_query($idkec, $idkel, $idpus)
    {
        $this->db->select('*');
        $this->db->join('tbl_penyakit tp', 'tp.id_penyakit = tt.id_penyakit');
        $this->db->from('tbl_terjangkit tt');
        $this->db->where('tt.id_kecamatan', $idkec);
        $this->db->where('tt.id_kelurahan', $idkel);
        $this->db->where('tt.id_puskesmas', $idpus);

        if (isset($_POST['arg']) && $_POST['arg'] != '') {
            $this->db->where('tt.id_penyakit', $_POST['arg']);
        }

        $this->db->order_by("tt.id_terjangkit", "DESC");
    }

    public function make_datatable($idkec, $idkel, $idpus)
    {
        $this->make_query($idkec, $idkel, $idpus);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_filtered_data($idkec, $idkel, $idpus)
    {
        $this->make_query($idkec, $idkel, $idpus);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function get_all_data($idkec, $idkel, $idpus)
    {
        $this->db->select("*");
        $this->db->from("tbl_terjangkit tt");
        $this->db->where('tt.id_kecamatan', $idkec);
        $this->db->where('tt.id_kelurahan', $idkel);
        $this->db->where('tt.id_puskesmas', $idpus);
        return $this->db->count_all_results();
    }
    // akhir datatable --------------------------------------------------------------------------------------

    public function getPenyakit()
    {
        return $this->db->get('tbl_penyakit')->result_array();
    }

    public function form_action($type)
    {
        $data = [
            'nama_lengkap' => $this->input->post('nama_lengkap', true),
            'no_hp' => $this->input->post('no_hp', true),
            'id_kecamatan' => $this->input->post('id_kecamatan', true),
            'id_kelurahan' => $this->input->post('id_kelurahan', true),
            'id_puskesmas' => $this->input->post('id_puskesmas', true),
            'id_penyakit' => $this->input->post('id_penyakit', true),
            'alamat' => $this->input->post('alamat', true),
        ];

        if ($type == 'add') {
            $this->db->insert('tbl_terjangkit', $data);
        } else {
            $this->db->where('id_terjangkit', $this->input->post('id_terjangkit', true));
            $this->db->update('tbl_terjangkit', $data);
        }

        if ($this->db->affected_rows() > 0) {
            $this->updateJmlTerjangkit($this->input->post('id_kecamatan', true), $this->input->post('id_kelurahan', true), $this->input->post('id_puskesmas', true));
            $this->updateTotalJmlTerjangkit($this->input->post('id_kecamatan', true));

            return true;
        } else {
            return false;
        }
    }

    public function getStatus($id)
    {
        $this->db->select('*');
        $this->db->join('tbl_kecamatan tk', 'tk.id_kecamatan = tt.id_kecamatan');
        $this->db->join('tbl_penyakit tp', 'tp.id_penyakit = tt.id_penyakit');
        $this->db->join('tbl_user tu', 'tu.id_user = tt.updatedby');
        $this->db->from('tbl_terjangkit tt');
        $this->db->where('tt.id_terjangkit', $id);

        return $this->db->get()->row_array();
    }

    public function getDetailTerjangkit($id)
    {
        $this->db->where('id_terjangkit', $id);
        return $this->db->get('tbl_terjangkit')->row_array();
    }

    public function updateJmlTerjangkit($idKec, $idkel, $idpus)
    {
        $jml_terjangkit = $this->db->query('SELECT COUNT(id_terjangkit) as jml_terjangkit FROM tbl_terjangkit WHERE id_kecamatan = ' . $idKec . ' AND id_kelurahan = ' . $idkel . ' AND id_puskesmas = ' . $idpus)->row_array();

        $this->db->set('jml_terjangkit', $jml_terjangkit['jml_terjangkit']);
        $this->db->where('id_kecamatan', $idKec);
        $this->db->where('id_kelurahan', $idkel);
        $this->db->where('id_puskesmas', $idpus);
        $this->db->update('tbl_puskesmas');
    }

    public function updateTotalJmlTerjangkit($idKec)
    {
        $this->db->where('id_kecamatan', $idKec);
        $jmlTerjangkit = $this->db->get('tbl_puskesmas')->result_array();

        $total = 0;
        foreach ($jmlTerjangkit as $row) {
            $total += intval($row['jml_terjangkit']);
        }


        if ($this->db->affected_rows() > 0) {
            $data = [
                'jml_terjangkit' => $total
            ];

            $this->db->where('id_kecamatan', $idKec);
            $this->db->update('tbl_kecamatan', $data);

            if ($this->db->affected_rows() > 0) {
                $this->setWarna($idKec, $total);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function setWarna($idkec, $total)
    {
        if ($total == 0) {
            $data['id_warna'] = 1;
        } else if ($total >= 1 && $total <= 5) {
            $data['id_warna'] = 2;
        } else if ($total >= 6 && $total <= 10) {
            $data['id_warna'] = 3;
        } else if ($total >= 11 && $total <= 15) {
            $data['id_warna'] = 4;
        } else if ($total >= 16 && $total <= 20) {
            $data['id_warna'] = 5;
        } else if ($total >= 21 && $total <= 25) {
            $data['id_warna'] = 6;
        } else if ($total >= 26) {
            $data['id_warna'] = 7;
        }

        $this->db->where('id_kecamatan', $idkec);
        $this->db->update('tbl_kecamatan', $data);

        return $data['id_warna'];
    }
}
