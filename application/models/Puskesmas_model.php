<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Puskesmas_model extends CI_Model
{
    // datatable --------------------------------------------------------------------------------------
    public function make_query($idkec, $idkel)
    {
        $this->db->select('*');
        $this->db->where('tp.id_kecamatan', $idkec);
        $this->db->where('tp.id_kelurahan', $idkel);
        $this->db->from('tbl_puskesmas tp');

        $this->db->order_by("tp.id_puskesmas", "DESC");
    }

    public function make_datatable($idkec, $idkel)
    {
        $this->make_query($idkec, $idkel);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_filtered_data($idkec, $idkel)
    {
        $this->make_query($idkec, $idkel);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function get_all_data($idkec, $idkel)
    {
        $this->db->select("*");
        $this->db->where('tp.id_kecamatan', $idkec);
        $this->db->where('tp.id_kelurahan', $idkel);
        $this->db->from("tbl_puskesmas tp");
        return $this->db->count_all_results();
    }
    // akhir datatable --------------------------------------------------------------------------------------

    public function form_action($type)
    {
        $data = [
            'nama_puskesmas' => $this->input->post('nama_puskesmas', true),
            'id_kecamatan' => $this->input->post('id_kecamatan', true),
            'id_kelurahan' => $this->input->post('id_kelurahan', true),
            'longitude' => $this->input->post('longitude', true),
            'latitude' => $this->input->post('latitude', true),
        ];

        if ($type == 'add') {
            $data['jml_terjangkit'] = 0;
            $this->db->insert('tbl_puskesmas', $data);
            return $this->db->insert_id();
        } else {
            $this->db->where('id_puskesmas', $this->input->post('id_puskesmas', true));
            $this->db->update('tbl_puskesmas', $data);
        }

        if ($this->db->affected_rows() > 0) {
            return $type == 'add' ? $this->db->insert_id() : true;
        } else {
            return false;
        }

        // $this->db->last_query();
    }

    public function getDetailPuskesmas($id)
    {
        $this->db->where('id_puskesmas', $id);
        return $this->db->get('tbl_puskesmas')->row_array();
    }

    // public function updateJmlTerjangkit($idpuskesmas)
    // {
    //     $this->db->set('jml_terjangkit', 'jml_terjangkit + 1');
    //     $this->db->where('id_puskesmas', $idpuskesmas);
    //     $this->db->update('tbl_puskesmas');
    // }
}
