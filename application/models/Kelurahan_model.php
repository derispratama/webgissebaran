<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kelurahan_model extends CI_Model
{
    // datatable --------------------------------------------------------------------------------------
    public function make_query($id_kecamatan)
    {
        $this->db->select('*');
        $this->db->from('tbl_kelurahan tk');
        $this->db->where('tk.id_kecamatan', $id_kecamatan);
        $this->db->order_by("tk.id_kelurahan", "DESC");
    }

    public function make_datatable($id_kecamatan)
    {
        $this->make_query($id_kecamatan);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_filtered_data($id_kecamatan)
    {
        $this->make_query($id_kecamatan);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function get_all_data($id_kecamatan)
    {
        $this->db->select("*");
        $this->db->from("tbl_kelurahan");
        $this->db->where('id_kecamatan', $id_kecamatan);
        return $this->db->count_all_results();
    }
    // akhir datatable --------------------------------------------------------------------------------------

    public function all_data()
    {
        $query = $this->db->get('tbl_kelurahan');
        return $query->result();
    }

    public function form_action($idkec, $type)
    {
        $data = [
            'nama_kelurahan' => $this->input->post('nama_kelurahan', true),
            'id_kecamatan' => $idkec,
        ];

        if ($type == 'add') {
            $this->db->insert('tbl_kelurahan', $data);
            return $this->db->insert_id();
        } else {
            $this->db->where('id_kelurahan', $this->input->post('id_kelurahan', true));
            $this->db->update('tbl_kelurahan', $data);
        }

        if ($this->db->affected_rows() > 0) {
            return $type == 'add' ? $this->db->insert_id() : true;
        } else {
            return false;
        }

        // $this->db->last_query();
    }

    public function getDetailKelurahan($id)
    {
        $this->db->where('id_kelurahan', $id);
        return $this->db->get('tbl_kelurahan')->row_array();
    }
}
