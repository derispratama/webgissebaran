<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kelurahan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Kelurahan_model');
        $this->load->library('form_validation');
    }

    public function index($id)
    {
        $kecamatan = $this->db->query('SELECT nama_kecamatan FROM tbl_kecamatan WHERE id_kecamatan = ' . $id)->row_array();
        $data = array(
            'title' => 'List Kelurahan',
            'menu' => 'Kecamatan/Kec. ' . $kecamatan['nama_kecamatan'],
            'idkec' => $id,
            'isi' => 'Kelurahan/v-Kelurahan-list',
            'urlTable' => base_url('kelurahan/fetch_ajax/' . $id),

        );
        $this->load->view('template/wrap', $data);
    }

    public function form_Kelurahan($idkec, $type = 'add', $id = '')
    {
        $kecamatan = $this->db->query('SELECT nama_kecamatan FROM tbl_kecamatan WHERE id_kecamatan = ' . $idkec)->row_array();
        $data = [
            'title' => $type == 'add' ? 'Form Tambah Kelurahan' : 'Form Edit Kelurahan',
            'wilayah' => $kecamatan,
            'menu' => 'Kecamatan/Kec. ' . $kecamatan['nama_kecamatan'],
            'idkec' => $idkec,
            'redirect' => base_url('kelurahan/index/' . $idkec),
            'formAction' => base_url('kelurahan/form_action/' . $idkec . '/' . $type),
            'isi' => 'Kelurahan/v-Kelurahan-form',

        ];

        if ($type == 'update') {
            $data['kelurahan'] = $this->Kelurahan_model->getDetailKelurahan($id);
        }
        $this->load->view('template/wrap', $data);
    }

    public function fetch_ajax($id_kecamatan)
    {
        $fetch_data = $this->Kelurahan_model->make_datatable($id_kecamatan);
        $data = [];
        $i = 1;

        foreach ($fetch_data as $row) {
            $subarray = [];
            $subarray[] = $i++;
            $subarray[] = $row->nama_kelurahan;
            $subarray[] = '<a class="btn btn-info btn-sm mr-1" href="' . base_url('puskesmas/list_puskesmas/' . $id_kecamatan . '/' . $row->id_kelurahan) . '" data-bs-toggle="tooltip" data-bs-placement="top" title="Detail"><i class="fa fa-search"></i>
             <a class="btn btn-warning btn-sm" style="background:#ff8f00;" href="' . base_url('kelurahan/form_kelurahan/' . $id_kecamatan . '/update/' . $row->id_kelurahan) . '" data-bs-toggle="tooltip" data-bs-placement="top" title="Update"><i class="fas fa-pencil-alt" style="color:white;"></i></a>  
            <a class="btn btn-danger btn-sm hapus" data-action="delete" href="#" data-f="' . $row->id_kelurahan . '" data-bs-toggle="tooltip" data-bs-placement="top" title="Delete"><i class="fa fa-trash"></i></a>';
            $data[] = $subarray;
        }

        $output = [
            "recordsTotal" => $this->Kelurahan_model->get_all_data($id_kecamatan),
            "recordsFiltered" => $this->Kelurahan_model->get_filtered_data($id_kecamatan),
            "data" => $data
        ];

        echo json_encode($output);
    }


    public function form_action($idkec, $type, $id = '')
    {
        $this->form_validation->set_rules('nama_kelurahan', 'Nama Kelurahan', 'trim|required');
        // $this->form_validation->set_rules('jml_penduduk', 'Jumlah Penduduk', 'trim|required|numeric');
        // $this->form_validation->set_rules('geojson', 'Geojson', 'trim|required');
        // $this->form_validation->set_rules('gambar', 'Gambar', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $msg = [
                'error' => true,
                'nama_kelurahan_error' => form_error('nama_kelurahan'),
                // 'jml_penduduk_error' => form_error('jml_penduduk'),
                // 'geojson_error' => form_error('geojson'),
                // 'gambar_error' => form_error('gambar'),
                'msg' => 'Periksa kembali Inputan anda'
            ];
            echo json_encode($msg);
        } else {



            $action = $this->Kelurahan_model->form_action($idkec, $type);
            $msg = '';
            $msgError = '';

            if ($action) {
                $msg = $type == 'add' ? ' Data has been saved' : 'Data has been updated';
            } else {
                $msgError = $type == 'add' ? ' Data failed to save' : 'Data failed to update';
            }

            $textWarning = $type == 'add' ? 'to add this data' : 'to update this data';

            $json = [
                'error' => false,
                'textWarning' => $textWarning,
                'msgError' => $msgError,
                'msg' => $msg,
            ];


            echo json_encode($json);
        }
    }
}
