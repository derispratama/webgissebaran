<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Puskesmas extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Puskesmas_model');
        $this->load->model('Terjangkit_model');
        $this->load->library('form_validation');
    }

    public function list_puskesmas($idkec, $idkel)
    {
        $m = $this->db->query('SELECT tk.nama_kecamatan,tl.nama_kelurahan FROM tbl_kelurahan tl INNER JOIN tbl_kecamatan tk ON tl.id_kecamatan = tk.id_kecamatan WHERE tl.id_kecamatan = ' . $idkec . ' AND tl.id_kelurahan = ' . $idkel)->row_array();

        $data = array(
            'title' => 'List Puskesmas',
            'menu' => 'Kecamatan/Kec. ' . $m['nama_kecamatan'] . '/Kel. ' . $m['nama_kelurahan'],
            'idkec' => $idkec,
            'idkel' => $idkel,
            'isi' => 'puskesmas/v-puskesmas-list',
            'urlTable' => base_url('puskesmas/fetch_ajax/' . $idkec . '/' . $idkel),
        );

        $warna = $this->Terjangkit_model->updateTotalJmlTerjangkit($idkec);

        $this->load->view('template/wrap', $data);
    }

    public function form_puskesmas($idkec, $idkel, $type = 'add', $id = '')
    {
        $m = $this->db->query('SELECT tk.nama_kecamatan,tl.nama_kelurahan FROM tbl_kelurahan tl INNER JOIN tbl_kecamatan tk ON tl.id_kecamatan = tk.id_kecamatan WHERE tl.id_kecamatan = ' . $idkec . ' AND tl.id_kelurahan = ' . $idkel)->row_array();

        $data = [
            'title' => $type == 'add' ? 'Form Tambah Puskesmas' : 'Form Edit Puskesmas',
            'menu' => 'Kecamatan/Kec. ' . $m['nama_kecamatan'] . '/Kel. ' . $m['nama_kelurahan'],
            'wilayah' => $m,
            'redirect' => base_url('puskesmas/list_puskesmas/' . $idkec . '/' . $idkel),
            'idkec' => $idkec,
            'idkel' => $idkel,
            'isi' => 'puskesmas/v-puskesmas-form',
            'action' => base_url('puskesmas/form_action/' . $type)
        ];

        if ($type == 'update') {
            $data['puskesmas'] = $this->Puskesmas_model->getDetailPuskesmas($id);
        }

        $this->load->view('template/wrap', $data);
    }

    public function fetch_ajax($idkec, $idkel)
    {
        $fetch_data = $this->Puskesmas_model->make_datatable($idkec, $idkel);
        $data = [];
        $i = 1;

        foreach ($fetch_data as $row) {
            $subarray = [];
            $subarray[] = $i++;
            $subarray[] = $row->nama_puskesmas;
            $subarray[] = $row->jml_terjangkit;
            $subarray[] = '<a class="btn btn-info btn-sm mr-1" href="' . base_url('terjangkit/list_terjangkit/' . $idkec . '/' . $idkel . '/' . $row->id_puskesmas) . '" data-bs-toggle="tooltip" data-bs-placement="top" title="Detail"><i class="fa fa-search"></i></a>
             <a class="btn btn-warning btn-sm" style="background:#ff8f00;" href="' . base_url('puskesmas/form_puskesmas/' . $idkec . '/' . $idkel . '/update/' . $row->id_puskesmas) . '" data-bs-toggle="tooltip" data-bs-placement="top" title="Update"><i class="fas fa-pencil-alt" style="color:white;"></i></a>  
            <a class="btn btn-danger btn-sm hapus" data-action="delete" href="#" data-f="' . $row->id_puskesmas . '" data-bs-toggle="tooltip" data-bs-placement="top" title="Delete"><i class="fa fa-trash"></i></a>';
            $data[] = $subarray;
        }

        $output = [
            "recordsTotal" => $this->Puskesmas_model->get_all_data($idkec, $idkel),
            "recordsFiltered" => $this->Puskesmas_model->get_filtered_data($idkec, $idkel),
            "data" => $data
        ];

        echo json_encode($output);
    }

    public function form_action($type)
    {
        $this->form_validation->set_rules('nama_puskesmas', 'Nama Kecamatan', 'trim|required');
        $this->form_validation->set_rules('latitude', 'Latitude', 'trim|required');
        $this->form_validation->set_rules('longitude', 'Longitude', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $msg = [
                'error' => true,
                'nama_puskesmas_error' => form_error('nama_puskesmas'),
                'latitude_error' => form_error('latitude'),
                'longitude_error' => form_error('longitude'),
                'msg' => 'Periksa kembali Inputan anda'
            ];
            echo json_encode($msg);
        } else {
            $action = $this->Puskesmas_model->form_action($type);
            $msg = '';
            $msgError = '';

            if ($action) {
                $msg = $type == 'add' ? ' Data has been saved' : 'Data has been updated';
            } else {
                $msgError = $type == 'add' ? ' Data failed to save' : 'Data failed to update';
            }

            $textWarning = $type == 'add' ? 'to add this data' : 'to update this data';

            $json = [
                'error' => false,
                'textWarning' => $textWarning,
                'msgError' => $msgError,
                'msg' => $msg,
            ];

            echo json_encode($json);
        }
    }
}
