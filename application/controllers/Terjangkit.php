<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Terjangkit extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Terjangkit_model');
        $this->load->library('form_validation');
    }

    public function list_terjangkit($idkec, $idkel, $idpus)
    {
        $m = $this->db->query('SELECT tk.nama_kecamatan,tl.nama_kelurahan,tp.nama_puskesmas FROM tbl_puskesmas tp INNER JOIN tbl_kelurahan tl ON tl.id_kelurahan = tp.id_kelurahan INNER JOIN tbl_kecamatan tk ON tp.id_kecamatan = tk.id_kecamatan WHERE tp.id_kecamatan = ' . $idkec . ' AND tp.id_kelurahan = ' . $idkel . ' AND tp.id_puskesmas = ' . $idpus)->row_array();

        $data = array(
            'title' => 'List Terjangkit',
            'menu' => 'Kecamatan/Kec. ' . $m['nama_kecamatan'] . '/Kel. ' . $m['nama_kelurahan'] . '/ Puskesmas. ' . $m['nama_puskesmas'],
            'isi' => 'terjangkit/v-terjangkit-list',
            'idkec' => $idkec,
            'idkel' => $idkel,
            'idpus' => $idpus,
            'urlTable' => base_url('terjangkit/fetch_ajax/' . $idkec . '/' . $idkel . '/' . $idpus),
            'penyakits' => $this->Terjangkit_model->getPenyakit(),
        );
        $this->load->view('template/wrap', $data);
    }

    public function form_terjangkit($idkec, $idkel, $idpus, $type = 'add', $id = '')
    {
        $m = $this->db->query('SELECT tk.nama_kecamatan,tl.nama_kelurahan,tp.nama_puskesmas FROM tbl_puskesmas tp INNER JOIN tbl_kelurahan tl ON tl.id_kelurahan = tp.id_kelurahan INNER JOIN tbl_kecamatan tk ON tp.id_kecamatan = tk.id_kecamatan WHERE tp.id_kecamatan = ' . $idkec . ' AND tp.id_kelurahan = ' . $idkel . ' AND tp.id_puskesmas = ' . $idpus)->row_array();

        $data = [
            'title' =>  $type == 'add' ? 'Form Tambah Terjangkit' : 'Form Edit Terjangkit',
            'menu' => 'Kecamatan/Kec. ' . $m['nama_kecamatan'] . '/Kel. ' . $m['nama_kelurahan'] . '/ Puskesmas. ' . $m['nama_puskesmas'],
            'wilayah' => $m,
            'formAction' => base_url('terjangkit/form_action/' . $type),
            'redirect' => base_url('terjangkit/list_terjangkit/' . $idkec . '/' . $idkel . '/' . $idpus),
            'penyakits' => $this->Terjangkit_model->getPenyakit(),
            'idkec' => $idkec,
            'idkel' => $idkel,
            'idpus' => $idpus,
            'isi' => 'terjangkit/v-terjangkit-form'
        ];

        if ($type == 'update') {
            $data['terjangkit'] = $this->Terjangkit_model->getDetailTerjangkit($id);
        }

        $this->load->view('template/wrap', $data);
    }

    public function form_action($type, $id = '')
    {
        $this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'trim|required');
        $this->form_validation->set_rules('no_hp', 'No Hp', 'trim|required|numeric');
        $this->form_validation->set_rules('id_penyakit', 'Terjangkit', 'trim|required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'trim');

        if ($this->form_validation->run() == FALSE) {
            $msg = [
                'error' => true,
                'nama_lengkap_error' => form_error('nama_lengkap'),
                'no_hp_error' => form_error('no_hp'),
                'id_penyakit_error' => form_error('id_penyakit'),
                'alamat_error' => form_error('alamat'),
                'msg' => 'Periksa kembali Inputan anda'
            ];
            echo json_encode($msg);
        } else {
            $action = $this->Terjangkit_model->form_action($type);
            $msg = '';
            $msgError = '';

            if ($action) {
                $error = false;
                $msg = $type == 'add' ? ' Data has been saved' : 'Data has been updated';
            } else {
                $error = true;
                $msg = $type == 'add' ? ' Data failed to save' : 'Data failed to update';
            }

            $textWarning = $type == 'add' ? 'to add this data' : 'to update this data';

            $json = [
                'error' => $error,
                'textWarning' => $textWarning,
                'msgError' => $msgError,
                'msg' => $msg,
            ];

            echo json_encode($json);
        }
    }

    public function fetch_ajax($idkec, $idkel, $idpus)
    {
        $fetch_data = $this->Terjangkit_model->make_datatable($idkec, $idkel, $idpus);
        $data = [];
        $i = 1;

        foreach ($fetch_data as $row) {
            $subarray = [];
            $subarray[] = $i++;
            $subarray[] = $row->nama_lengkap;
            $subarray[] = $row->no_hp;
            $subarray[] = $row->alamat;

            if ($row->id_penyakit == 1) {
                $bg = 'danger';
            } else {
                $bg = 'warning';
            }

            // $subarray[] = '<button type="button" class="btn btn-' . $bg . ' btn-modal-status" data-id="' . $row->id_terjangkit . '" data-toggle="modal" data-target="#modal-detail-terjangkit"><i class="fa fa-list-alt"></i></button>';
            $subarray[] = '<span class="badge bg-' . $bg . '">' . $row->nama_penyakit . '</span>';
            $subarray[] = '
             <a class="btn btn-warning btn-sm" style="background:#ff8f00;" href="' . base_url('terjangkit/form_terjangkit/' . $idkec . '/' . $idkel . '/' . $idpus . '/update/' . $row->id_terjangkit) . '" data-bs-toggle="tooltip" data-bs-placement="top" title="Update"><i class="fas fa-pencil-alt" style="color:white;"></i></a>  
            <a href="#" class="btn btn-danger btn-sm" data-bs-toggle="tooltip" data-bs-placement="top" title="Delete" onclick="btn_delete(' . $row->id_terjangkit . ',`terjangkit/delete_terjangkit`,`terjangkit`,' . $row->id_kecamatan . ',' . $row->id_kelurahan . ',' . $row->id_puskesmas . ')"><i class="fa fa-trash"></i></a>';
            $data[] = $subarray;
        }

        $output = [
            "recordsTotal" => $this->Terjangkit_model->get_all_data($idkec, $idkel, $idpus),
            "recordsFiltered" => $this->Terjangkit_model->get_filtered_data($idkec, $idkel, $idpus),
            "data" => $data
        ];

        echo json_encode($output);
    }

    public function delete_terjangkit()
    {
        $this->db->where('id_terjangkit', intval($_POST['data']['f']));
        $this->db->delete('tbl_terjangkit');

        if ($this->db->affected_rows() > 0) {
            $this->Terjangkit_model->updateJmlTerjangkit($_POST['data']['id_kecamatan'], $_POST['data']['id_kelurahan'], $_POST['data']['id_puskesmas']);
            $this->Terjangkit_model->updateTotalJmlTerjangkit($_POST['data']['id_kecamatan']);

            $json = [
                'msg' => 'Data berhasil dihapus',
                'status' => true
            ];
        } else {
            $json = [
                'msg' => 'Data gagal dihapus',
                'status' => false
            ];
        }
        echo json_encode($json);
    }

    public function getStatus()
    {
        $id = $this->input->post('id', true);
        $return = $this->Terjangkit_model->getStatus($id);
        echo json_encode($return);
    }
}
