<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kecamatan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Kecamatan_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data = array(
            'title' => 'List Kecamatan',
            'isi' => 'kecamatan/v-kecamatan-list',
            'menu' => 'Kecamatan',
            'urlTable' => base_url('kecamatan/fetch_ajax'),
        );
        $this->load->view('template/wrap', $data);
    }

    public function form_kecamatan($type = 'add', $id = '')
    {
        $data = [
            'title' => $type == 'add' ? 'Form Tambah Kecamatan' : 'Form Edit Kecamatan',
            'redirect' => base_url('kecamatan'),
            'menu' => 'Kecamatan',
            'isi' => 'kecamatan/v-kecamatan-form'
        ];
        $this->load->view('template/wrap', $data);

        // if (isset($_POST['submit'])) {
        //     $this->form_action($type);
        // }
    }

    private function upload($file)
    {
        $config['upload_path']          = './uploads/geojson';
        $config['allowed_types']        = '*';
        // $config2['allowed_types']        = 'jpeg|jpg|png';
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if (!$this->upload->do_upload('geojson')) {
            $data['geojson'] = array('error' => $this->upload->display_errors());
        } else {
            $data['geojson'] = array('upload_data' => $this->upload->data());
        }


        // if ($file['gambar']) {
        //     $config2['upload_path']          = './uploads/gambar';
        //     $config2['allowed_types']        = 'jpeg|jpg|png';
        //     $this->load->library('upload', $config2);
        //     $this->upload->initialize($config2);

        //     if (!$this->upload->do_upload('gambar')) {
        //         $data['gambar'] = array('error' => $this->upload->display_errors());
        //     } else {
        //         $data['gambar'] = array('upload_data' => $this->upload->data());
        //     }
        // }


        return $data;
    }

    public function fetch_ajax()
    {
        $fetch_data = $this->Kecamatan_model->make_datatable();
        $data = [];
        $i = 1;

        foreach ($fetch_data as $row) {
            $subarray = [];
            $subarray[] = $i++;
            $subarray[] = $row->nama_kecamatan;
            $subarray[] = $row->jml_terjangkit;
            $subarray[] = '<a class="btn btn-info btn-sm mr-1" href="' . base_url('kelurahan/index/' . $row->id_kecamatan) . '" data-bs-toggle="tooltip" data-bs-placement="top" title="Detail"><i class="fa fa-search"></i>
             <a class="btn btn-warning btn-sm" style="background:#ff8f00;" href="' . base_url('kecamatan/form_kecamatan/update/' . $row->id_kecamatan) . '" data-bs-toggle="tooltip" data-bs-placement="top" title="Update"><i class="fas fa-pencil-alt" style="color:white;"></i></a>  
            <a class="btn btn-danger btn-sm hapus" data-action="delete" href="#" data-f="' . $row->id_kecamatan . '" data-bs-toggle="tooltip" data-bs-placement="top" title="Delete"><i class="fa fa-trash"></i></a>';
            $data[] = $subarray;
        }

        $output = [
            "recordsTotal" => $this->Kecamatan_model->get_all_data(),
            "recordsFiltered" => $this->Kecamatan_model->get_filtered_data(),
            "data" => $data
        ];

        echo json_encode($output);
    }


    public function form_action($type)
    {
        $this->form_validation->set_rules('nama_kecamatan', 'Nama Kecamatan', 'trim|required');
        // $this->form_validation->set_rules('jml_penduduk', 'Jumlah Penduduk', 'trim|required|numeric');
        // $this->form_validation->set_rules('geojson', 'Geojson', 'trim|required');
        // $this->form_validation->set_rules('gambar', 'Gambar', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $msg = [
                'error' => true,
                'nama_kecamatan_error' => form_error('nama_kecamatan'),
                // 'jml_penduduk_error' => form_error('jml_penduduk'),
                // 'geojson_error' => form_error('geojson'),
                // 'gambar_error' => form_error('gambar'),
                'msg' => 'Periksa kembali Inputan anda'
            ];
            echo json_encode($msg);
        } else {
            $upload = $this->upload($_FILES);

            if (isset($upload['geojson']['error']) && isset($upload['geojson']['error'])) {
                $uploadErr = true;
            } else if (isset($upload['geojson']['error']) || isset($upload['geojson']['error'])) {
                $uploadErr = true;
            } else {
                $uploadErr = false;
            }

            if (!$uploadErr) {
                $action = $this->Kecamatan_model->form_action($type);
                $msg = '';
                $msgError = '';

                if ($action) {
                    $msg = $type == 'add' ? ' Data has been saved' : 'Data has been updated';
                } else {
                    $msgError = $type == 'add' ? ' Data failed to save' : 'Data failed to update';
                }

                $textWarning = $type == 'add' ? 'to add this data' : 'to update this data';

                $json = [
                    'error' => false,
                    'textWarning' => $textWarning,
                    'msgError' => $msgError,
                    'msg' => $msg,
                ];
            } else {
                $json = [
                    'error' => true,
                    'textWarning' => '',
                    'msgError' => 'upload error',
                    'msg' => '',
                ];
            }

            echo json_encode($json);
        }
    }
}
