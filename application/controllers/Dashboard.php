<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Dashboard_model');
		$this->load->model('Kecamatan_model');

    }

    public function index()
    {
        $data = array(
            'title' => 'Dashboard',
            'isi' => 'v-dashboard',
            'menu' => 'Dashboard',
			'kecamatan' => $this->Kecamatan_model->all_data(),
            'chart' => true
        );
        $this->load->view('template/wrap', $data);
    }

    public function getDataChart()
    {
        $chart = $this->Dashboard_model->getDataChart();

        echo json_encode($chart);
    }

    public function puskesmas_json()
	{
		$puskesmasdetail = $this->db->query("SELECT tp.*,SUM(CASE WHEN tt.id_penyakit = '1' THEN 1 ELSE '0' END) as giziburuk_puskesmas, SUM(CASE WHEN tt.id_penyakit = '2' THEN 1 ELSE '0' END) as stunting_puskesmas FROM tbl_puskesmas tp LEFT JOIN tbl_terjangkit tt ON tt.id_puskesmas = tp.id_puskesmas GROUP BY tp.id_puskesmas")->result_array();

		echo json_encode($puskesmasdetail);
	}
}
