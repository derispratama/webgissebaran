<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penduduk extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Penduduk_model');
        $this->load->library('form_validation');
    }

    public function list_penduduk($id)
    {
        $data = array(
            'title' => 'List Penduduk',
            'kecamatan' => $this->db->query('SELECT nama_kecamatan FROM tbl_kecamatan WHERE id_kecamatan = ' . $id)->row_array(),
            'isi' => 'penduduk/v-penduduk-list',
            'idkec' => $id,
            'urlTable' => base_url('penduduk/fetch_ajax/' . $id),
        );
        $this->load->view('template/wrap', $data);
    }

    public function form_penduduk($idkec, $type = 'add', $id = '')
    {
        $data = [
            'title' =>  $type == 'add' ? 'Form Tambah Penduduk' : 'Form Edit Penduduk',
            'formAction' => base_url('penduduk/form_action/' . $idkec . '/' . $type),
            'redirect' => base_url('penduduk/list_penduduk/' . $idkec),
            'penyakits' => $this->Penduduk_model->getPenyakit(),
            'isi' => 'penduduk/v-penduduk-form'
        ];

        if ($type == 'update') {
            $data['penduduk'] = $this->Penduduk_model->getDetailPenduduk($id);
        }

        $this->load->view('template/wrap', $data);
    }

    public function form_action($idkec, $type, $id = '')
    {
        $this->form_validation->set_rules('nik', 'NIK', 'trim|required|numeric');
        $this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'trim|required');
        $this->form_validation->set_rules('no_hp', 'No Hp', 'trim|required|numeric');
        $this->form_validation->set_rules('id_penyakit', 'Terjangkit', 'trim|required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'trim');

        if ($this->form_validation->run() == FALSE) {
            $msg = [
                'error' => true,
                'nik_error' => form_error('nik'),
                'nama_lengkap_error' => form_error('nama_lengkap'),
                'no_hp_error' => form_error('no_hp'),
                'id_penyakit_error' => form_error('id_penyakit'),
                'alamat_error' => form_error('alamat'),
                'msg' => 'Periksa kembali Inputan anda'
            ];
            echo json_encode($msg);
        } else {
            $action = $this->Penduduk_model->form_action($idkec, $type);
            $msg = '';
            $msgError = '';

            if ($action) {
                $msg = $type == 'add' ? ' Data has been saved' : 'Data has been updated';
            } else {
                $msgError = $type == 'add' ? ' Data failed to save' : 'Data failed to update';
            }

            $textWarning = $type == 'add' ? 'to add this data' : 'to update this data';

            $json = [
                'error' => false,
                'textWarning' => $textWarning,
                'msgError' => $msgError,
                'msg' => $msg,
            ];

            echo json_encode($json);
        }
    }

    public function fetch_ajax($id_kecamatan)
    {
        $fetch_data = $this->Penduduk_model->make_datatable($id_kecamatan);
        $data = [];
        $i = 1;

        foreach ($fetch_data as $row) {
            $subarray = [];
            $subarray[] = $i++;
            $subarray[] = $row->nik;
            $subarray[] = $row->nama_lengkap;

            if ($row->id_penyakit == 0) {
                $bg = 'success';
            } else if ($row->id_penyakit == 1 || $row->id_penyakit == 2) {
                $bg = 'warning';
            } else {
                $bg = 'danger';
            }

            $subarray[] = '<button type="button" class="btn btn-' . $bg . ' btn-modal-status" data-id="' . $row->id_penduduk . '" data-toggle="modal" data-target="#modal-detail-penduduk"><i class="fa fa-list-alt"></i></button>';
            $subarray[] = '<span class="badge bg-' . $bg . '">' . $row->nama_penyakit . '</span>';
            $subarray[] = '
             <a class="btn btn-warning btn-sm" style="background:#ff8f00;" href="' . base_url('penduduk/form_penduduk/' . $id_kecamatan . '/update/' . $row->id_penduduk) . '" data-bs-toggle="tooltip" data-bs-placement="top" title="Update"><i class="fas fa-pencil-alt" style="color:white;"></i></a>  
            <a class="btn btn-danger btn-sm hapus" data-action="delete" href="#" data-f="' . $row->id_penduduk . '" data-bs-toggle="tooltip" data-bs-placement="top" title="Delete"><i class="fa fa-trash"></i></a>';
            $data[] = $subarray;
        }

        $output = [
            "recordsTotal" => $this->Penduduk_model->get_all_data($id_kecamatan),
            "recordsFiltered" => $this->Penduduk_model->get_filtered_data($id_kecamatan),
            "data" => $data
        ];

        echo json_encode($output);
    }

    public function getStatus()
    {
        $id = $this->input->post('id', true);
        $return = $this->Penduduk_model->getStatus($id);
        echo json_encode($return);
    }
}
