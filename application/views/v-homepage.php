<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>DINKES KOTA BANDUNG : </title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- Leaflet -->
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin="" />
  <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
  <script>
   const url = '<?= base_url(); ?>';
 </script>
</head>

<body class="hold-transition layout-top-nav">
  <div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand-md navbar-light   navbar-white">
      <div class="container">
        <a href="<?= base_url() ?>assets/index3.html" class="navbar-brand">
          <img src="<?= base_url() ?>assets/gambar/hospital.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
          <span class="brand-text font-weight-light"><b style="color:Gold; font-weight:900;">DINKES</b> <b style="color:Green; font-weight:900;">KOTA</b> <b style="color:Blue; font-weight:900;">BANDUNG</b></span>
        </a>

        <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse order-3" id="navbarCollapse">
          <!-- <ul class="navbar-nav">
            <li class="nav-item">
              <a href="index3.html" class="nav-link">Home</a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link">Contact</a>
            </li>
            <li class="nav-item dropdown">
              <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Dropdown</a>
              <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
                <li><a href="#" class="dropdown-item">Some action </a></li>
                <li><a href="#" class="dropdown-item">Some other action</a></li>

                <li class="dropdown-divider"></li>

                <li class="dropdown-submenu dropdown-hover">
                  <a id="dropdownSubMenu2" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">Hover for action</a>
                  <ul aria-labelledby="dropdownSubMenu2" class="dropdown-menu border-0 shadow">
                    <li>
                      <a tabindex="-1" href="#" class="dropdown-item">level 2</a>
                    </li>

                    <li class="dropdown-submenu">
                      <a id="dropdownSubMenu3" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">level 2</a>
                      <ul aria-labelledby="dropdownSubMenu3" class="dropdown-menu border-0 shadow">
                        <li><a href="#" class="dropdown-item">3rd level</a></li>
                        <li><a href="#" class="dropdown-item">3rd level</a></li>
                      </ul>
                    </li>

                    <li><a href="#" class="dropdown-item">level 2</a></li>
                    <li><a href="#" class="dropdown-item">level 2</a></li>
                  </ul>
                </li>
              </ul>
            </li>
          </ul> -->
        </div>

        <!-- Right navbar links -->
        <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
          <li class="nav-item">
            <a href="<?= site_url('login') ?>" class="nav-link">Login</a>
          </li>
        </ul>
      </div>
    </nav>
    <!-- /.navbar -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">

      </div>
      <!-- /.content-header -->

      <!-- Carousel
      <div class="content">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                      <div class="carousel-item active">
                        <img class="d-block w-100" src="<?= base_url() ?>assets/gambar/contoh1.png" alt="First slide">
                      </div>
                      <div class="carousel-item">
                        <img class="d-block w-100" src="https://placehold.it/900x500/3c8dbc/ffffff&text=I+Love+Bootstrap" alt="Second slide">
                      </div>
                      <div class="carousel-item">
                        <img class="d-block w-100" src="https://placehold.it/900x500/f39c12/ffffff&text=I+Love+Bootstrap" alt="Third slide">
                      </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      End Carousel -->

      <section class="content">
        <div class="container">
          <div class="row">
            <div class="col-md-4">
              <!-- PIE CHART -->
              <div class="card card-danger">
                <div class="card-header">
                  <h3 class="card-title">Pie Chart</h3>

                </div>
                <div class="card-body">
                  <canvas id="pieChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <div class="col-md-8">
              <!-- BAR CHART -->
              <div class="card card-success">
                <div class="card-header">
                  <h3 class="card-title">Bar Chart</h3>

                </div>
                <div class="card-body">
                  <div class="chart">
                    <canvas id="barChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                  </div>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
          </div>
        </div>
      </section>

      <!-- Peta -->
      <div class="content">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h3 class="card-title">Peta Sebaran GIZI BURUK & STUNTING</h3>
                </div>
                <div class="card-body">
                  <div id="mapid" style="width: 100%; height: 400px;"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      
            <div class="card bg-secondary mb-3" style="width: 70rem; height: 107vh; display: flex; margin: auto;">
            <div class="card-header" style="text-align: center; font-family: system-ui; font-size: 32px;">Apa Yang Harus Dilakukan ?</div>
            <div class="card-body">
                <h5 class="card-title" style="font-size: 25px;"><b>Mencegah Stunting dan Gizi Buruk</b></h5>
                <br><br>
                <p class="card-text" style="font-size: 20px; text-align:justify; text-justify:auto;">&emsp;&emsp;&emsp;Diakibatkan oleh asupan gizi yang kurang, mencegah Stunting tentu dapat dilakukan dengan memenuhi kebutuhan gizi yang sesuai. Namun, yang menjadi pertanyaan adalah, bagaimana jalan yang paling tepat agar kebutuhan gizi dapat tercukupi dengan baik?

                    Dampak Stunting umumnya terjadi karena diakibatkan oleh kurangnya asupan nutrisi pada 1.000 hari pertama anak. Hitungan 1.000 hari di sini dimulai sejak janin sampai anak berusia 2 tahun.

                    Jika pada rentang waktu ini, gizi tidak dicukupi dengan baik, dampak yang ditimbulkan memiliki efek jangka pendek dan efek jangka panjang. Gejala stunting jangka pendek meliputi hambatan perkembangan, penurunan fungsi kekebalan, penurunan fungsi kognitif, dan gangguan sistem pembakaran.</p> 
                    <br>
                    <p class="card-text" style="font-size: 20px; text-align:justify; text-justify:auto;">&emsp;&emsp;&emsp;Sedangkan gejala jangka panjang meliputi obesitas, penurunan toleransi glukosa, penyakit jantung koroner, hipertensi, dan osteoporosis.

                    Oleh karena itu, upaya pencegahan baiknya dilakukan sedini mungkin. Pada usia 1.000 hari pertama kehidupan, asupan nutrisi yang baik sangat dianjurkan dikonsumsi oleh ibu hamil. Tidak hanya untuk mencukupi kebutuhan nutrisi dirinya, asupan nutrisi yang baik juga dibutuhkan jabang bayi yang ada dalam kandungannya.</p>
                    <br>
                    <p class="card-text" style="font-size: 20px; text-align:justify; text-justify:auto;">&emsp;&emsp;&emsp;Lebih lanjut, pada saat bayi telah lahir, penelitian <b>untuk mencegah Stunting</b> menunjukkan bahwa, konsumsi protein sangat mempengaruhi pertambahan tinggi dan berat badan anak di atas 6 bulan.

                    Anak yang mendapat asupan protein 15 persen dari total asupan kalori yang dibutuhkan terbukti memiliki badan lebih tinggi dibanding anak dengan asupan protein 7,5 persen dari total asupan kalori.

                    Anak usia 6 sampai 12 bulan dianjurkan mengonsumsi protein harian sebanyak 1,2 g/kg berat badan. Sementara anak usia 1 – 3 tahun membutuhkan protein harian sebesar 1,05 g/kg berat badan. Jadi, pastikan si kecil mendapat asupan protein yang cukup sejak ia pertama kali mencicipi makanan padat pertamanya.</p>
            </div>
            </div>
            
            <div class="card bg-light mb-3" style="width: 70rem; height: 175vh; display: flex; margin-left: auto; margin-right: auto">
                <img class="card-img-top" src="<?= base_url() ?>assets/gambar/stunting.jpg" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title" style="font-size: 32px; font-family: system-ui; transform:translateX(26rem)"><b>Penyebab Stunting</b></h5><br><br><br>
                    <p class="card-text" style="font-size: 20px; text-align:justify; text-justify:auto;">&emsp;&emsp;&emsp;Situs Adoption Nutrition menyebutkan, stunting berkembang dalam jangka panjang karena kombinasi dari beberapa atau semua faktor-faktor berikut:</p>
                    <p class="card-text" style="font-size: 20px; text-align:justify; text-justify:auto; color: goldenrod;">&emsp;&emsp;&emsp;<b>1. Kurang gizi kronis dalam waktu lama</b></p>
                    <p class="card-text" style="font-size: 20px; text-align:justify; text-justify:auto; color: slategrey">&emsp;&emsp;&emsp;<b>2. Retardasi pertumbuhan intrauterine</b></p>
                    <p class="card-text" style="font-size: 20px; text-align:justify; text-justify:auto; color: olive">&emsp;&emsp;&emsp;<b>3. Tidak cukup protein dalam proporsi total asupan kalori</b></p>
                    <p class="card-text" style="font-size: 20px; text-align:justify; text-justify:auto; color: sienna">&emsp;&emsp;&emsp;<b>4. Perubahan hormon yang dipicu oleh stres</b></p>
                    <p class="card-text" style="font-size: 20px; text-align:justify; text-justify:auto; color: slateblue">&emsp;&emsp;&emsp;<b>5. Sering menderita infeksi di awal kehidupan seorang anak</b></p><br>
                    <p class="card-text" style="font-size: 20px; text-align:justify; text-justify:auto;">&emsp;&emsp;&emsp;Perkembangan stunting adalah proses yang lambat, kumulatif dan tidak berarti bahwa asupan makanan saat ini tidak memadai. Kegagalan pertumbuhan mungkin telah terjadi di masa lalu seorang.</p>
                </div>
                </div>
                <div class="card bg-warning" style="width: 70rem; height: 172vh; display: flex; margin-left: auto; margin-right: auto">
                <div class="card-body">
                    <h5 class="card-title" style="font-size: 32px; font-family: system-ui; transform:translateX(13rem)"><b>Cara Dinkes Mengatasi Stunting dan Gizi Buruk</b></h5><br><br><br>
                    <p class="card-text" style="font-size: 20px; text-align:justify; text-justify:auto;">&emsp;&emsp;&emsp;Pemerintah Kota (Pemkot) Bandung berupaya melindungi warganya darigizi buruk dan stunting dari hulu hingga hilir. Salah satu upaya yang dilakukan adalah, memberikan edukasi dan melancarkan program pencegahan kepada warga tentang kedua masalah gizi tersebut. Kepala Bidang Kesehatan Masyarakat Dinas Kesehatan (Dinskes) Kota Bandung, Henny Rahayu mengungkapkan, ada tiga kelompok rentan terkena masalah gizi, yaitu bayi, balita, dan ibu hamil. Oleh karena itu penanganannya harus secara menyeluruh.</p><br>
                    <p class="card-text" style="font-size: 20px; text-align:justify; text-justify:auto;">&emsp;&emsp;&emsp; Kita merancang program antisipasi dan pencegahan gizi buruk sejak jauh sebelum kehamilan hingga setelah bayi lahir. Karena persoalan gizi ini kompleks dan panjang. Untuk mengatasi masalah gizi buruk dan stunting, Dinkes Kota Bandung melancarkan empat program komprehensif sejak para calon ibu masih remaja hingga setelah anak lahir sampai beranjak balita. Program tersebut dimulai dengan pemberian vitamin penambah zat besi kepada siswi SMP dan SMA setiap seminggu sekali selama satu tahun.</p><br>
                    <p class="card-text" style="font-size: 20px; text-align:justify; text-justify:auto;">&emsp;&emsp;&emsp;Selain itu, Dinkes Kota Bandung bekerja sama dengan Kantor Kementerian Agama Kota Bandung mengedukasi tentang kesehatan ibu dan anak kepada para calon pasangan yang akan menikah. Setiap ada yang mendaftar ke Kantor Urusan Agama (KUA), Dinkes akan memberikan penyuluhan. Tak cukup hanya di situ, Dinkes melalui UPT Puskesmas di seluruh kecamatan juga membuka kelas ibu hamil dan ibu balita. Rata-rata, setiap ibu hamil yang memeriksakan kandungannya ke Puskesmas akan menerima 3 kali pertemuan tentang berbagai materi kehamilan. Mulai dari soal kehamilan, cara membuat makanan bayi dan balita, hingga senam ibu hamil.</p>
                </div>
                <img class="card-img-bottom" src="<?= base_url() ?>assets/gambar/stun.jpg" style="max-height: 72vh" alt="Card image cap">
                </div>

                <div class="card bg-dark mb-3" style="width: 70rem; height: 60vh; display: flex; margin-left: auto; margin-right: auto">
            <div class="card-header" style="text-align: center; font-family: system-ui; font-size: 32px;"><b>Daftar Rumah Sakit Rujukan di Kota Bandung</b></div>
            <div class="card-body">
                <h5 class="card-title" style="font-size: 20px; text-align:justify; text-justify:auto;">&emsp;&emsp;&emsp;Berikut ini adalah rumah sakit yang menjadi rujukan untuk pasien stunting dan gizi buruk dengan status Pasien dalam Pengawasan. Anda harus mengunjungi fasilitas kesehatan terdekat terlebih dahulu seperti puskesmas, klinik, dan rumah sakit umum sebelum akhirnya dapat dirujuk ke rumah sakit khusus di bawah ini.</h5>
                <br>    
                </p> <br> <br> <ul class="hospital-list"><li><div id="0" class="mb-8 pl-5 relative"><div class="absolute top-0 bottom-0 left-0 w-1 bg-orange-300"></div> <h5 class="inline-block text-lg font-bold leading-loose">
                RSU Dr. Hasan Sadikin
                </h5> <p> Jl. Pasteur No. 38, Kota Bandung </p> <p class="text-sm"><a href="tel:+62 22 2034 953" target="_blank" title="Telpon +62 22 2034 953" class="inline-block px-4 py-1 bg-blue-100 rounded mt-2 mr-2 text-gray-800 hover:opacity-50"><i class="fas fa-phone fa-sm mr-1"></i> <span>
                    +62 22 2034 953
                    </span></a><a href="tel:+62 22 2034 954" target="_blank" title="Telpon +62 22 2034 954" class="inline-block px-4 py-1 bg-blue-100 rounded mt-2 mr-2 text-gray-800 hover:opacity-50"><i class="fas fa-phone fa-sm mr-1"></i> <span>
                    +62 22 2034 954
                    </span></a><a href="tel:+62 22 2034 955" target="_blank" title="Telpon +62 22 2034 955" class="inline-block px-4 py-1 bg-blue-100 rounded mt-2 mr-2 text-gray-800 hover:opacity-50"><i class="fas fa-phone fa-sm mr-1"></i> <span>
                    +62 22 2034 955
                    </span></a><a href="tel:+62 22 2551 111" target="_blank" title="Telpon +62 22 2551 111" class="inline-block px-4 py-1 bg-blue-100 rounded mt-2 mr-2 text-gray-800 hover:opacity-50"><i class="fas fa-phone fa-sm mr-1"></i> <span>
                    +62 22 2551 111
                    </span></a> <a href="https://web.rshs.or.id" target="_blank" title="Buka Laman https://web.rshs.or.id" class="inline-block px-4 py-1 bg-green-100 rounded mt-2 mr-2 text-gray-800 hover:opacity-50"><i class="fas fa-globe-asia fa-sm mr-1"></i> <span>
                    https://web.rshs.or.id
                </span></a></p></div></li><li><div id="1" class="mb-8 pl-5 relative"><div class="absolute top-0 bottom-0 left-0 w-1 bg-orange-300"></div> <h5 class="inline-block text-lg font-bold leading-loose">
                RSTP Dr. H. A. Rotinsulu
                </h5> <p>Jl. Bukit Jarian No.40, Hegarmanah, Kota Bandung </p> <p class="text-sm"><a href="tel:+62 22 2034446" target="_blank" title="Telpon +62 22 2034446" class="inline-block px-4 py-1 bg-blue-100 rounded mt-2 mr-2 text-gray-800 hover:opacity-50"><i class="fas fa-phone fa-sm mr-1"></i> <span>
                    +62 22 2034446
                    </span></a> <a href="http://rsparurotinsulu.org" target="_blank" title="Buka Laman http://rsparurotinsulu.org" class="inline-block px-4 py-1 bg-green-100 rounded mt-2 mr-2 text-gray-800 hover:opacity-50"><i class="fas fa-globe-asia fa-sm mr-1"></i> <span>
                    http://rsparurotinsulu.org
                </span></a></p></div></li>
            </div>
            </div>
            <br>
    </div>
    
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
      <div class="p-3">
        <h5>Title</h5>
        <p>Sidebar content</p>
      </div>
    </aside>
    <!-- /.control-sidebar -->
    <br>
    <!-- Main Footer -->
    <footer class="main-footer" style="margin-top: -40px;">
      <!-- To the right -->
      <div class="float-right d-none d-sm-inline">
        Anything you want
      </div>
      <!-- Default to the left -->
      <strong>Copyright &copy; 2021 <a href="#">DINAS KESEHATAN KOTA BANDUNG</a>.</strong> All rights reserved.
    </footer>
  </div>
  <!-- ./wrapper -->

  <!-- REQUIRED SCRIPTS -->

  <!-- jQuery -->
  <script src="<?= base_url() ?>assets/plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="<?= base_url() ?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE App -->
  <script src="<?= base_url() ?>assets/dist/js/adminlte.min.js"></script>
  <!-- ChartJS -->
  <script src="<?= base_url() ?>assets/plugins/chart.js/Chart.min.js"></script>

  <!-- Leaflet -->
  <script>
    var mymap = L.map('mapid').setView([-6.919801128234722, 107.63354551245905], 11);

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
      maxZoom: 18,
      attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, ' +
        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
      id: 'mapbox/streets-v11',
      tileSize: 512,
      zoomOffset: -1
    }).addTo(mymap);

    <?php foreach ($kecamatan as $key => $value) { ?>
      $.getJSON("<?= base_url('uploads/geojson/' . $value->geojson) ?>", function(data) {
        geoLayer = L.geoJson(data, {
          style: function(feature) {
            return {
              opacity: 1.0,
              color: 'black',
              fillOpacity: 0.6,
              fillColor: '<?= $value->code_warna ?>',

            }
          },
        }).addTo(mymap);

        geoLayer.eachLayer(function(layer) {
          <?php if ($value->statuskecamatan == 'Aman') : ?>
            const badge = 'badge bg-success';
          <?php elseif ($value->statuskecamatan == 'Siaga') : ?>
            const badge = 'badge bg-warning';
          <?php else : ?>
            const badge = 'badge bg-danger';
          <?php endif; ?>

          layer.bindPopup(
            "Kecamatan : <b><?= $value->nama_kecamatan ?></b><br>" +
            "Jumlah Terjangkit : <?= $value->jml_terjangkit ?><br>" +
            "Gizi Buruk : <?= $value->giziburuk_kecamatan ?><br>" +
            "Stunting : <?= $value->stunting_kecamatan ?><br>" +
            "<span class='" + badge + "'>" + '<?= $value->statuskecamatan; ?>' + "</span>"
          );
        });
      });

      $.getJSON("<?= base_url() ?>homepage/puskesmas_json", function(data) {
        $.each(data, function(i, field) {
          var v_long = parseFloat(data[i].longitude);
          var v_lat = parseFloat(data[i].latitude);
          var icon_puskesmas = L.icon({
              iconUrl: "<?= base_url()?>assets/gambar/location.png",
              iconSize: [50, 50]
          });
          L.marker([v_long, v_lat], {icon:icon_puskesmas}).addTo(mymap)
            .bindPopup(`
            <h6>${data[i].nama_puskesmas}</h6>  <br>
            Jumlah Terjangkit : ${data[i].jml_terjangkit} <br>
            Gizi Buruk : ${data[i].giziburuk_puskesmas} <br>
            Stunting : ${data[i].stunting_puskesmas}<br>
            `)
            .openPopup();
        });
      });




    <?php } ?>
  </script>
 <script src="<?= base_url('assets/js/chart.js'); ?>"></script>

<script>
   <?php if (isset($chart)) : ?>
     barChart();
   <?php endif; ?>
 </script>
</body>

</html>