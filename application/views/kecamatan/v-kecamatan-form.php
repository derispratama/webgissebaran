<div class="row">
    <div class="col-md-12">

        <a href="<?= base_url('kecamatan'); ?>" class="btn btn-secondary mb-2"><i class="fa fa-arrow-alt-circle-left"></i> Back</a>
        <div class="card">

            <div class="card-body">
                <form action="<?= base_url('kecamatan/form_action/add') ?>" method="post" id="form" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Nama Kecamatan</label>
                                <input type="text" name="nama_kecamatan" class="form-control" placeholder="Enter Nama Kecamatan" value="" autocomplete="off">
                                <small class="text text-danger" id="nama_kecamatan_error"></small>
                            </div>
                            <div class="form-group">
                                <label>Geojson</label>
                                <input type="file" name="geojson" class="form-control" autocomplete="off">
                                <small class="text text-danger" id="geojson_error"></small>
                            </div>
                            <div class="form-group" style="display: flex;justify-content:flex-end;">
                                <button type="submit" name="submit" id="btn-submit" class="btn btn-primary">Save</button>
                                <button type="reset" class="btn btn-danger ml-1"><i class="icon-x"></i> Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>