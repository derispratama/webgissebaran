 <!-- Main Footer -->
 <footer class="main-footer">
   <!-- To the right -->
   <div class="float-right d-none d-sm-inline">
     Anything you want
   </div>
   <!-- Default to the left -->
   <strong>Copyright &copy; 2021 <a href="#">DINAS KESEHATAN KOTA BANDUNG</a>.</strong> All rights reserved.
 </footer>
 </div>
 <!-- ./wrapper -->

 <!-- Modal -->
 <div class="modal fade" id="modal-detail-terjangkit" tabindex="-1" aria-labelledby="modal-terjangkit" aria-hidden="true">
   <div class="modal-dialog">
     <div class="modal-content">
       <div class="modal-header">
         <h5 class="modal-title" id="modal-terjangkit">Detail</h5>
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
           <span aria-hidden="true">&times;</span>
         </button>
       </div>
       <div class="modal-body">
         <!--  -->
       </div>
       <div class="modal-footer">
         <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
       </div>
     </div>
   </div>
 </div>

 <!-- REQUIRED SCRIPTS -->
 <script>
   const urlHapus = '<?= isset($urlHapus) ? $urlHapus : '' ?>';
   const urlTable = '<?= isset($urlTable) ? $urlTable : '' ?>';
   const redirect = '<?= isset($redirect) ? $redirect : '' ?>';
   const url = '<?= base_url(); ?>';
 </script>
 <!-- jQuery -->
 <script src="<?= base_url() ?>assets/plugins/jquery/jquery.min.js"></script>
 <!-- Bootstrap 4 -->
 <script src="<?= base_url() ?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
 <!-- chart -->
 <script src="<?= base_url('assets/plugins/chart.js/Chart.min.js'); ?>"></script>
 <!-- AdminLTE App -->
 <script src="<?= base_url() ?>assets/dist/js/adminlte.min.js"></script>
 <!-- sweetalert -->
 <script src="<?= base_url() ?>assets/plugins/sweetalert2/sweetalert2.min.js"></script>
 <!-- datatable -->
 <script src="<?= base_url('assets/plugins/datatables/jquery.dataTables.min.js') ?>"></script>
 <script src="<?= base_url('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') ?>"></script>
 <!-- /datatable -->
 <!-- my script -->
 <script src="<?= base_url('assets/js/form.js'); ?>"></script>
 <script src="<?= base_url('assets/js/table.js'); ?>"></script>
 <script src="<?= base_url('assets/js/chart.js'); ?>"></script>

 <script>
   <?php if (isset($chart)) : ?>
     barChart();
   <?php endif; ?>
 </script>
 </body>

 </html>