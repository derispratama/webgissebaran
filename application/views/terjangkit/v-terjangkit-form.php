<div class="row">
    <div class="col-md-12">
        <a href="<?= base_url('terjangkit/list_terjangkit/' . $idkec . '/' . $idkel . '/' . $idpus); ?>" class="btn btn-secondary mb-2"><i class="fa fa-arrow-alt-circle-left"></i> Back</a>
        <div class="card">
            <div class="card-body">
                <form action="<?= $formAction; ?>" method="post" id="form" enctype="multipart/form-data">
                    <input type="hidden" name="id_terjangkit" value="<?= isset($terjangkit) ? $terjangkit['id_terjangkit'] : ''; ?>" readonly>
                    <input type="hidden" name="id_kecamatan" value="<?= $idkec; ?>" readonly>
                    <input type="hidden" name="id_kelurahan" value="<?= $idkel; ?>" readonly>
                    <input type="hidden" name="id_puskesmas" value="<?= $idpus; ?>" readonly>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Kecamatan</label>
                                <input type="text" class="form-control" placeholder="Enter Nama Kelurahan" autocomplete="off" value="<?= $wilayah['nama_kecamatan'] ?>" readonly>
                            </div>
                            <div class="form-group">
                                <label>Kelurahan</label>
                                <input type="text" class="form-control" placeholder="Enter Nama Kelurahan" autocomplete="off" value="<?= $wilayah['nama_kelurahan'] ?>" readonly>
                            </div>
                            <div class="form-group">
                                <label>Puskesmas</label>
                                <input type="text" class="form-control" placeholder="Enter Nama Kelurahan" autocomplete="off" value="<?= $wilayah['nama_puskesmas'] ?>" readonly>
                            </div>
                            <div class="form-group">
                                <label>Nama Lengkap</label>
                                <input type="text" name="nama_lengkap" class="form-control" placeholder="Enter nama lengkap" value="<?= isset($terjangkit) ? $terjangkit['nama_lengkap'] : ''; ?>" autocomplete="off">
                                <small class="text text-danger" id="nama_lengkap_error"></small>
                            </div>
                            <div class="form-group">
                                <label>No Hp</label>
                                <input type="text" name="no_hp" class="form-control" placeholder="Enter no hp" value="<?= isset($terjangkit) ? $terjangkit['no_hp'] : ''; ?>" autocomplete="off">
                                <small class="text text-danger" id="no_hp_error"></small>
                            </div>
                            <div class="form-group">
                                <label>Terjangkit</label>
                                <select name="id_penyakit" id="id_penyakit" class="form-control">
                                    <option value="">== Pilih Penyakit ==</option>
                                    <?php foreach ($penyakits as $penyakit) : ?>
                                        <?php if ($penyakit['id_penyakit'] == $terjangkit['id_penyakit']) : ?>
                                            <option value="<?= $penyakit['id_penyakit'] ?>" selected><?= $penyakit['nama_penyakit'] ?></option>
                                        <?php else : ?>
                                            <option value="<?= $penyakit['id_penyakit'] ?>"><?= $penyakit['nama_penyakit'] ?></option>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Alamat</label>
                                <textarea name="alamat" id="alamat" class="form-control" cols="30" rows="10"><?= isset($terjangkit) ? $terjangkit['alamat'] : ''; ?></textarea>
                                <small class="text text-danger" id="alamat_error"></small>
                            </div>
                            <div class="form-group" style="display: flex;justify-content:flex-end;">
                                <button type="submit" name="submit" id="btn-submit" class="btn btn-primary">Save</button>
                                <button type="reset" class="btn btn-danger ml-1"><i class="icon-x"></i> Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>