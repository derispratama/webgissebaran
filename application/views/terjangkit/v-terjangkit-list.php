<div class="row">
    <div class="col-md-7">
        <div class="card p-2">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Penyakit</label><br>
                        <select name="fid_penyakit" id="fid_penyakit" class="form-control">
                            <option value="">== ALL Penyakit ==</option>
                            <?php foreach ($penyakits as $penyakit) : ?>
                                <option value="<?= $penyakit['id_penyakit'] ?>"><?= $penyakit['nama_penyakit'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">&nbsp;</label><br>
                        <button type="button" id="btn-filter-penyakit" class="btn btn-success"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <a href="<?= base_url('terjangkit/form_terjangkit/' . $idkec . '/' . $idkel . '/' . $idpus . '/add'); ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Add Penduduk</a>
                <a href="<?= base_url('puskesmas/list_puskesmas/' . $idkec . '/' . $idkel); ?>" class="btn btn-secondary"><i class="fa fa-arrow-alt-circle-left"></i> Back</a>
            </div>
            <div class="card-body">
                <table id="table" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Lengkap</th>
                            <th>No Hp</th>
                            <th>Alamat</th>
                            <!-- <th>Detail</th> -->
                            <th>Status Penyakit</th>
                            <th>Aksi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>