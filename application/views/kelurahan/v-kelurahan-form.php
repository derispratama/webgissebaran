<div class="row">
    <div class="col-md-12">
        <a href="<?= base_url('kelurahan/index/' . $idkec); ?>" class="btn btn-secondary mb-2"><i class="fa fa-arrow-alt-circle-left"></i> Back</a>
        <div class="card">
            <div class="card-body">
                <form action="<?= $formAction ?>" method="post" id="form" enctype="multipart/form-data">
                    <input type="hidden" name="id_kelurahan" value="<?= isset($kelurahan) ? $kelurahan['id_kelurahan'] : ''; ?>" readonly>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Kecamatan</label>
                                <input type="text" class="form-control" placeholder="Enter Nama Kelurahan" autocomplete="off" value="<?= $wilayah['nama_kecamatan'] ?>" readonly>
                            </div>
                            <div class="form-group">
                                <label>Nama Kelurahan</label>
                                <input type="text" name="nama_kelurahan" class="form-control" placeholder="Enter Nama Kelurahan" value="<?= isset($kelurahan) ? $kelurahan['nama_kelurahan'] : ''; ?>" autocomplete="off" autofocus>
                                <small class="text text-danger" id="nama_kelurahan_error"></small>
                            </div>
                            <div class="form-group" style="display: flex;justify-content:flex-end;">
                                <button type="submit" name="submit" id="btn-submit" class="btn btn-primary">Save</button>
                                <button type="reset" class="btn btn-danger ml-1"><i class="icon-x"></i> Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>