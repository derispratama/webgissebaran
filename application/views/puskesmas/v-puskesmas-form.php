<div class="row">
    <div class="col-md-12">
        <a href="<?= base_url('puskesmas/list_puskesmas/' . $idkec . '/' . $idkel); ?>" class="btn btn-secondary mb-2"><i class="fa fa-arrow-alt-circle-left"></i> Back</a>
        <div class="card">
            <div class="card-body">
                <form action="<?= $action; ?>" method="post" id="form" enctype="multipart/form-data">
                    <input type="hidden" name="id_puskesmas" value="<?= isset($puskesmas) ? $puskesmas['id_puskesmas'] : '' ?>" readonly>
                    <input type="hidden" name="id_kecamatan" value="<?= $idkec; ?>" readonly>
                    <input type="hidden" name="id_kelurahan" value="<?= $idkel; ?>" readonly>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Kecamatan</label>
                                <input type="text" class="form-control" placeholder="Enter Nama Kelurahan" autocomplete="off" value="<?= $wilayah['nama_kecamatan'] ?>" readonly>
                            </div>
                            <div class="form-group">
                                <label>Kelurahan</label>
                                <input type="text" class="form-control" placeholder="Enter Nama Kelurahan" autocomplete="off" value="<?= $wilayah['nama_kelurahan'] ?>" readonly>
                            </div>
                            <div class="form-group">
                                <label>Nama Puskesmas</label>
                                <input type="text" name="nama_puskesmas" class="form-control" placeholder="Nama Puskesmas" autocomplete="off" autofocus value="<?= isset($puskesmas) ? $puskesmas['nama_puskesmas'] : ''; ?>">
                                <small class="text text-danger" id="nama_puskesmas_error"></small>
                            </div>
                            <div class="form-group">
                                <label>Longitude</label>
                                <input type="text" name="longitude" class="form-control" placeholder="Longitude" autocomplete="off" value="<?= isset($puskesmas) ? $puskesmas['longitude'] : ''; ?>">
                                <small class="text text-danger" id="longitude_error"></small>
                            </div>
                            <div class="form-group">
                                <label>Latitude</label>
                                <input type="text" name="latitude" class="form-control" placeholder="Latitude" autocomplete="off" value="<?= isset($puskesmas) ? $puskesmas['latitude'] : ''; ?>">
                                <small class="text text-danger" id="latitude_error"></small>
                            </div>
                            <div class="form-group" style="display: flex;justify-content:flex-end;">
                                <button type="submit" name="submit" id="btn-submit" class="btn btn-primary">Save</button>
                                <button type="reset" class="btn btn-danger ml-1"><i class="icon-x"></i> Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>