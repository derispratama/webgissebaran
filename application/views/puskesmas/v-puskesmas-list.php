<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <a href="<?= base_url('puskesmas/form_puskesmas/' . $idkec . '/' . $idkel . '/add'); ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Add Puskesmas</a>
                <a href="<?= base_url('kelurahan/index/' . $idkec); ?>" class="btn btn-secondary"><i class="fa fa-arrow-alt-circle-left"></i> Back</a>
            </div>
            <div class="card-body">
                <table id="table" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Puskesmas</th>
                            <th>Jumlah Terjangkit</th>
                            <th>Aksi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>