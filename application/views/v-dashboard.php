            <!-- BAR CHART -->
            <div class="card card-success">
                <div class="card-header">
                    <h3 class="card-title">Bar Chart</h3>
                </div>
                <div class="card-body">
                    <div class="chart">
                        <canvas id="barChart" style="height:500px; min-height:500px"></canvas>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- Peta -->
            <div class="content">
                <div class="container">
                <div class="row">
                    <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                        <h3 class="card-title">Peta Sebaran GIZI BURUK & STUNTING</h3>
                        </div>
                        <div class="card-body">
                        <div id="mapid" style="width: 100%; height: 400px;"></div>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            </div>

            <script>
                var mymap = L.map('mapid').setView([-6.919801128234722, 107.63354551245905], 11);

                L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
                maxZoom: 18,
                attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, ' +
                    'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                id: 'mapbox/streets-v11',
                tileSize: 512,
                zoomOffset: -1
                }).addTo(mymap);

                <?php foreach ($kecamatan as $key => $value) { ?>
                    $.getJSON("<?= base_url('uploads/geojson/' . $value->geojson) ?>", function(data) {
                        geoLayer = L.geoJson(data, {
                            style: function(feature) {
                                return {
                                opacity: 1.0,
                                color: 'black',
                                fillOpacity: 0.6,
                                fillColor: '<?= $value->code_warna ?>',
                                }
                            },
                        }).addTo(mymap);

                        geoLayer.eachLayer(function(layer) {
                        <?php if ($value->statuskecamatan == 'Aman') : ?>
                            const badge = 'badge bg-success';
                        <?php elseif ($value->statuskecamatan == 'Siaga') : ?>
                            const badge = 'badge bg-warning';
                        <?php else : ?>
                            const badge = 'badge bg-danger';
                        <?php endif; ?>

                        layer.bindPopup(
                            "Kecamatan : <b><?= $value->nama_kecamatan ?></b><br>" +
                            "Jumlah Terjangkit : <?= $value->jml_terjangkit ?><br>" +
                            "Gizi Buruk : <?= $value->giziburuk_kecamatan ?><br>" +
                            "Stunting : <?= $value->stunting_kecamatan ?><br>" +
                            "<span class='" + badge + "'>" + '<?= $value->statuskecamatan; ?>' + "</span>"
                            );
                         });
                    });

                    $.getJSON("<?= base_url() ?>homepage/puskesmas_json", function(data) {
                        $.each(data, function(i, field) {
                        var v_long = parseFloat(data[i].longitude);
                        var v_lat = parseFloat(data[i].latitude);
                        L.marker([v_long, v_lat]).addTo(mymap)
                            .bindPopup(`
                            Nama Puskesmas : ${data[i].nama_puskesmas} <br>
                            Jumlah Terjangkit : ${data[i].jml_terjangkit} <br>
                            Gizi Buruk : ${data[i].giziburuk_puskesmas} <br>
                            Stunting : ${data[i].stunting_puskesmas}<br>
                            `)
                            .openPopup();
                        });
                    });

                <?php } ?>
            </script>