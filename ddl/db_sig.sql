-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 23, 2021 at 08:03 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sig`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kecamatan`
--

CREATE TABLE `tbl_kecamatan` (
  `id_kecamatan` int(11) NOT NULL,
  `nama_kecamatan` varchar(100) NOT NULL,
  `jml_penduduk` int(11) NOT NULL,
  `geojson` varchar(255) NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `updatedby` int(11) NOT NULL,
  `updateddate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_kecamatan`
--

INSERT INTO `tbl_kecamatan` (`id_kecamatan`, `nama_kecamatan`, `jml_penduduk`, `geojson`, `gambar`, `status`, `updatedby`, `updateddate`) VALUES
(3, 'Lengkong', 4, 'lengkong.geojson', 'lengkong.jpg', 1, 1, '2021-04-20 23:13:07'),
(6, 'Babakan Ciparay', 1, 'bacip.geojson', 'bacip.jpg', 1, 1, '2021-06-23 12:33:09');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_penduduk`
--

CREATE TABLE `tbl_penduduk` (
  `id_penduduk` int(11) NOT NULL,
  `id_kecamatan` int(11) NOT NULL,
  `id_penyakit` int(11) NOT NULL,
  `nik` int(11) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `no_hp` varchar(20) NOT NULL,
  `alamat` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `updatedby` int(11) NOT NULL,
  `updateddate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_penduduk`
--

INSERT INTO `tbl_penduduk` (`id_penduduk`, `id_kecamatan`, `id_penyakit`, `nik`, `nama_lengkap`, `no_hp`, `alamat`, `status`, `updatedby`, `updateddate`) VALUES
(3, 3, 1, 123456, 'Azka', '08945646', 'alamat', 1, 1, '2021-06-23 07:52:55'),
(4, 3, 2, 12, 'as', '12', 'a', 1, 1, '2021-06-22 14:21:17'),
(5, 3, 0, 1212, 'derasasasasasas', '1212', 'alamat', 1, 1, '2021-06-23 07:17:32'),
(6, 3, 3, 3333, 'jska', '1212', 'alamat', 1, 1, '2021-06-23 07:17:41'),
(7, 6, 1, 1212, 'dfdf', '21313213', 'alamat', 1, 1, '2021-06-23 07:53:04');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_penyakit`
--

CREATE TABLE `tbl_penyakit` (
  `id_penyakit` int(11) NOT NULL,
  `nama_penyakit` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `updatedby` int(11) NOT NULL,
  `updateddate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_penyakit`
--

INSERT INTO `tbl_penyakit` (`id_penyakit`, `nama_penyakit`, `status`, `updatedby`, `updateddate`) VALUES
(0, 'sehat', 1, 1, '2021-04-20 23:31:11'),
(1, 'gizi buruk', 1, 1, '2021-04-20 22:29:43'),
(2, 'stunting', 1, 1, '2021-04-20 22:29:43'),
(3, 'komplikasi', 1, 1, '2021-06-22 18:56:18');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_role`
--

CREATE TABLE `tbl_role` (
  `role_id` int(11) NOT NULL,
  `rolename` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_role`
--

INSERT INTO `tbl_role` (`role_id`, `rolename`) VALUES
(1, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id_user` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `username` varchar(10) NOT NULL,
  `fullname` varchar(20) NOT NULL,
  `nohp` varchar(20) NOT NULL,
  `alamat` text NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id_user`, `role_id`, `username`, `fullname`, `nohp`, `alamat`, `password`, `status`) VALUES
(1, 1, 'tao123', 'Taofik Setiawan', '089121212', 'alamat', 'tao123', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_warna`
--

CREATE TABLE `tbl_warna` (
  `id_warna` int(11) NOT NULL,
  `range` int(11) NOT NULL,
  `code_warna` varchar(50) NOT NULL,
  `updatedby` int(11) NOT NULL,
  `updateddate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_warna`
--

INSERT INTO `tbl_warna` (`id_warna`, `range`, `code_warna`, `updatedby`, `updateddate`) VALUES
(1, 0, '#a7ffeb ', 1, '2021-04-20 22:23:51'),
(2, 10, '#a7ffeb ', 1, '2021-04-20 22:23:51'),
(3, 20, '#a7ffeb ', 1, '2021-04-20 22:25:26'),
(4, 30, '#a7ffeb ', 1, '2021-04-20 22:25:45'),
(5, 40, '#a7ffeb', 1, '2021-04-20 22:26:10'),
(6, 50, '#a7ffeb', 1, '2021-04-20 22:26:10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_kecamatan`
--
ALTER TABLE `tbl_kecamatan`
  ADD PRIMARY KEY (`id_kecamatan`),
  ADD KEY `updatedby` (`updatedby`);

--
-- Indexes for table `tbl_penduduk`
--
ALTER TABLE `tbl_penduduk`
  ADD PRIMARY KEY (`id_penduduk`),
  ADD KEY `id_kecamatan` (`id_kecamatan`),
  ADD KEY `id_penyakit` (`id_penyakit`),
  ADD KEY `updatedby` (`updatedby`);

--
-- Indexes for table `tbl_penyakit`
--
ALTER TABLE `tbl_penyakit`
  ADD PRIMARY KEY (`id_penyakit`),
  ADD KEY `updatedby` (`updatedby`);

--
-- Indexes for table `tbl_role`
--
ALTER TABLE `tbl_role`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `role_id` (`role_id`);

--
-- Indexes for table `tbl_warna`
--
ALTER TABLE `tbl_warna`
  ADD PRIMARY KEY (`id_warna`),
  ADD KEY `updatedby` (`updatedby`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_kecamatan`
--
ALTER TABLE `tbl_kecamatan`
  MODIFY `id_kecamatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_penduduk`
--
ALTER TABLE `tbl_penduduk`
  MODIFY `id_penduduk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_penyakit`
--
ALTER TABLE `tbl_penyakit`
  MODIFY `id_penyakit` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_role`
--
ALTER TABLE `tbl_role`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_warna`
--
ALTER TABLE `tbl_warna`
  MODIFY `id_warna` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_kecamatan`
--
ALTER TABLE `tbl_kecamatan`
  ADD CONSTRAINT `tbl_kecamatan_ibfk_2` FOREIGN KEY (`updatedby`) REFERENCES `tbl_user` (`id_user`);

--
-- Constraints for table `tbl_penduduk`
--
ALTER TABLE `tbl_penduduk`
  ADD CONSTRAINT `tbl_penduduk_ibfk_1` FOREIGN KEY (`id_kecamatan`) REFERENCES `tbl_kecamatan` (`id_kecamatan`),
  ADD CONSTRAINT `tbl_penduduk_ibfk_2` FOREIGN KEY (`id_penyakit`) REFERENCES `tbl_penyakit` (`id_penyakit`),
  ADD CONSTRAINT `tbl_penduduk_ibfk_3` FOREIGN KEY (`updatedby`) REFERENCES `tbl_user` (`id_user`);

--
-- Constraints for table `tbl_penyakit`
--
ALTER TABLE `tbl_penyakit`
  ADD CONSTRAINT `tbl_penyakit_ibfk_1` FOREIGN KEY (`updatedby`) REFERENCES `tbl_user` (`id_user`);

--
-- Constraints for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD CONSTRAINT `tbl_user_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `tbl_role` (`role_id`);

--
-- Constraints for table `tbl_warna`
--
ALTER TABLE `tbl_warna`
  ADD CONSTRAINT `tbl_warna_ibfk_1` FOREIGN KEY (`updatedby`) REFERENCES `tbl_user` (`id_user`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
